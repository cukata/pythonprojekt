from pygame import *

window = display.set_mode((700, 500)) # zobrazi okno o velikosti 700, 500
display.set_caption("Nase okno") # nastavi popisek okna

NasObrazek = image.load("smile.png") # nacte obrazek
background = transform.scale(NasObrazek, (100, 100)) # nacte a upravi velikost obrazku do promene background

x = 300
y = 200

game = True
while game:

    clock = time.Clock()
    clock.tick(60)

    keys_pressed = key.get_pressed()

    if keys_pressed[K_DOWN] :
        y += 5
    elif keys_pressed[K_UP] :
        y -= 5
    elif keys_pressed[K_LEFT] :
        x -= 5
    elif keys_pressed[K_RIGHT] :
        x += 5

    for e in event.get():
        if e.type == QUIT:
            game = False

    window.fill((255, 255, 255))
    window.blit(background, (x, y)) # zobarazi v nasem okne na pozici 0, 0
    display.update()